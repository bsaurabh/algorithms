import java.util.Iterator;
public class Stack <Item> implements Iterable <Item>
{
	public Iterator<Item> iterable()
	{
		return new ReverseArrayIterator();
	}

	private class ReverseArrayIterator implements Iterator<Item>
	{
		private int current = N;

		public boolean hasNext()
		{
			return current>0;
		}

		public void remove()
		{
			// Not supported
		}

		public Item next()
		{
			return s[--current];
		}
	}

	private Item s[];
	private int N;

	public Stack()
	{
		s = (Item[]) new Object[1];
		N = 0;	// point of insertion
	}

	public boolean isEmpty()
	{
		return (N == 0);
	}

	private void resize(int capacity)
	{
		Item[] copy = (Item[]) new Object[capacity];
		for (int i = 0; i < N; i++)
			copy[i] = s[i];
		s = copy;
	}

	public void push(Item item)
	{
		if (N == s.length)
			resize(s.length * 2);
		s[N++] = item;
	}

	public Item pop()
	{
		Item item = s[--N];
		s[N] = null;	// prevent loitering
		if (N > 0 && (N == (s.length / 4)))
			resize(s.length/2);
		return item;
	}
}
