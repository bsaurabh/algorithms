public class Graph
{
	private final int V;	// immutable number of vertices
	private Bag<Integer>[] adj;	// stores adjacency list of each vertex
	
	public int getVertexCount()
	{
		return V;
	}

	public Graph(int V)
	{
		this.V = V;
		adj = (Bag<Integer>) new Bag[V];	// generic array creation not allowed
		for (int i = 0; i < V; i++)
			adj[i] = new Bag<Integer>();	// initialise every adjacency list
	}

	public void addEdge(int v, int w)
	{
		adj[v].add(w);
		adj[w].add(v);
	}

	public Iterable<Integer> adj(int v)
	{
		return adj[v];
	}
}
