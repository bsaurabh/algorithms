public class ResizingArrayStackOfStrings
{
	private String s[];
	private int N;
	
	public ResizingArrayStackOfStrings()
	{
		s = new String[1];
		N = 0;	// position where push happens
	}

	public boolean isEmpty()
	{
		return (N == 0);
	}
	
	private void resize(int capacity)
	{
		String copy[] = new String[capacity];
		// copy all elements
		for (int i = 0; i < N; i++)
			copy[i] = s[i];
		s = copy;
	}

	public void push(String str)
	{
		if (N == s.length)	// array is full
			resize(2 * N);
		s[N++] = str;	
	}

	public String pop()
	{
		String item = s[--N];
		s[N] = null;
		// resize
		if (N > 0 && N == s.length/4)	// prevent thrashing
			resize(s.length/2);	// invariant: always between 25% and 100%
		return item;
	}

}
