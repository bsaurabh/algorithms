public class QuickFindUF
{
	private int id[];

	public QuickFindUF(int N)
	{
		id = new int[N];
		for (int i = 0; i < N; i++)
			id[i] = i;	// each elem is in its own connected component
	}

	public boolean connected(int p, int q)
	{
		return (id[p] == id[q]);
	}

	public void union(int p, int q)
	{
		/* change all elements having id = id[p]
		 * to having id = id[q]
		 */
		int pid = id[p];
		int qid = id[q];
		for (int i = 0; i < id.length; i++)
			if (id[i] == pid)
				id[i] = qid;
	}
}
