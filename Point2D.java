public class Point2D
{
	public final double x, y;

	public Point2D(double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	public int ccw(Point2D a, Point2D b, Point2D c)
	{
		// get signed area with determinant
		int area = (b.x - a.x)*(c.y - a.y) - (b.y - a.y)(c.x - a.x);

		if (area > 0)
			return +1;	// counter-clockwise
		else if (area < 0)
			return -1;
		else
			return 0;
	}

}
