public class BST <Key extends Comparable<Key>, Value>
{
	// BSTs correspond exactly to quicksort's partitioning
	
	private Node root;	// points to the BST

	private class Node
	{
		private Key key;
		private Value val;
		private Node left, right;
		private int count;	// number of nodes in subtrees+itself
		public Node(Key key, Value val)
		{
			this.key = key;
			this.val = val;
			this.count = 1;
		}
	}

	public void put(Key key, Value val)
	{
		root = put(root, key, val);
	}

	private Node put(Node x, Key key, Value val)
	{
		if (x == null)
			return new Node(key, val);
		int cmp = key.compareTo(x.key);
		if (cmp < 0)
			x.left = put(x.left, key, val);
		else if(cmp > 0)
			x.right = put(x.right, key, val);
		else
			x.val = val;
		x.count = 1 + size(x.left) + size(x.right);
		return x;
	}

	public Value get(Key key)
	{
		Node x = root;
		while (x != null)
		{
			int cmp = key.compareTo(x.key);
			if (cmp < 0)
				x = x.left;
			else if (cmp > 0)
				x = x.right;
			else
				return x.val;
		}
		return null;
	}

	public void delete(Key key)
	{
		// Hibbard Deletion
		// Makes height = root(n) after a long chain of insert-deletes
		// This is an unbalanced deletion
		root = delete(root, key);		
	}

	private Node delete(Node x, Key key)
	{
		if (x == null)	return null;
		int cmp = key.compareTo(x.key);
		if (cmp < 0)	x.left = delete(x.left, key);
		else if (cmp > 0)	x.right = delete(x.right, key);
		else
		{
			// key found
			if (x.right == null)
				return x.left;
			Node t = x;
			x = min(t.right);	// get the minimum element of right subtree
			x.right = deleteMin(x.right);	// get right tree to point to original right without minimum
			x.left = t.left;	// get left tree to point to original left
		}
		// fix count
		x.count =  1 + size(x.left) + size(x.right);
		return x;	// fixes parent link
	}

	private Node min(Node x)
	{
		if (x == null)	return null;
		while (x.left != null)
			x = x.left;
		return x;
	}

	public Iterable<Key> iterator()
	{
		// to be implemented
	}

	public Key floor(Key key)
	{
		// return the floor of the key
		Node x = floor(root, key);
		if (x == null)
			return null;
		return x.key;
	}

	private Node floor(Node x, Key key)
	{
		/*
		 * If key is found: it is its own floor
		 * If key < x.key -> floor is in left subtree
		 * If key > x.key -> floor is either x or is in right subtree
		 */
		if (x == null)
			return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0)
			return x;	// key itself is found...it is its own floor
		if (cmp < 0)
			return floor(x.left, key);
		// if key is greater, x is a candidate
		Node tmp = floor(x.right, key);
		if (tmp == null)
			return x;
		return tmp;
	}

	public Node ceil(Key key)
	{
		Node x = ceil(root, key);
		if (x == null)
			return null;
		return x;
	}

	private Node ceil(Node x, Key key)
	{
		if (x == null)
			return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0)
			return x;
		if (cmp > 0)
			return ceil(x.right, key);
		Node tmp = ceil(x.left, key);
		if (tmp == null)
			return x;
		return tmp;
	}

	public int size()
	{
		return size(root);
	}

	private int size(Node x)
	{
		if (x==null)
			return 0;
		return x.count;
	}

	public int rank(Key key)
	{
		// how many keys < key
		return (rank(root, key));
	}

	private int rank(Node x, Key key)
	{
		if (x == null)	return 0;
		int cmp = key.compareTo(x.key);
		if (cmp < 0)
			return rank(x.left, key);
		else if (cmp > 0)
			return (1 + size(x.left) + rank(x.right, key));
		else
			return (size(x.left));
	}

	public Iterable<Key> keys()
	{
		// returns sorted list of keys
		// inorder traversal of BST is done to yield keys in natural order
		Queue<Key> q = new Queue<Key>();
		inorder(root, q);
		return q;
	}

	private void inorder(Node x, Queue<Key> q)
	{
		if (x == null)	return;
		inorder(x.left, q);
		q.enqueue(x);
		inorder(x.right, q);
	}

	public void deleteMin()
	{
		root = deleteMin(root);
	}

	private Node deleteMin(Node x)
	{
		if (x.left == null)
			return x.right;	// minimum element's right subtree returned
		x.left = deleteMin(x.left);
		x.count = 1 + size(x.left) + size(x.right);
		return x;
	}
}
