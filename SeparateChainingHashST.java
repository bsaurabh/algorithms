public class SeparateChainingHashST<Key, Value>
{
	private int M = 97;	// length of hash table
	private Node[] st = new Node[M];	// hash table

	private static class Node
	{
		Object key;
		Object val;
		Node next;

		public Node(Key key, Value val, Node next)
		{
			this.key = key;
			this.val = val;
			this.next = next;
		}
	}

	private int hash(Key key)
	{
		// Use system hashing...transform to our requirements
		// Equal probabality between 0 and M
		// not -2^31 and 2^31 - 1
		return (key.hashCode() & 0x7fffffff) % M;	
		// firtst part ensures positive hash value
	}

	public Value get(Key key)
	{
		int i = hash(key);
		// look into st[i]
		for(Node x = st[i]; x != null; x = x.next)
			if (key.equals(x.key))
				return (Value)x.val;
		return null;
	}

	public void put(Key key, Value val)
	{
		int i = hash(key);
		for (Node x = st[i]; x != null; x = x.next)
			if (x.key.equals(key))
			{
				x.val = val;
				return;
			}
		// else create a new node...link to beginning of list
		st[i] = new Node(key, val, st[i]);
	}

}
