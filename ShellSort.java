public class ShellSort
{
	public static void sort (Comparable[] a)
	{
		int N = a.length;

		// use 3x+1 sequence
		int h = 1;
		while (h < N/3)
			h = 3*h + 1;	// when this breaks out h ~ N (nearest 3 multiple)

		// use h-sort (modified insertion sort)
		while (h >= 1)	// loop to perform different h-sorts
		{
			// regular insertion sort here
			for (int i = h; i < N; i++)	// always start from h: trivial
			{
				for (int j = i; j >= h; j -= h)
					if (less(a[j], a[j-h]))
						exch(a, j, j-h);
			}
			h = h / 3;
		}
	}

	private static boolean less(Comparable v, Comparable w)
	{
		return (v.compareTo(w) < 0);
	}

	private static void exch(Comparable[] a, int i, int j)
	{
		Comparable swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}
}
