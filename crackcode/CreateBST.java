public class CreateBST
{
	// given a sorted array create a BST
	public static Node createBST(int[] arr)
	{
		return createBST(arr, 0, a.length-1);
	}

	private static Node createBST(int[] arr, int lo, int hi)
	{
		if (hi <= lo)	return;
		int mid = (lo + hi)/2;
		Node n = new Node(arr[mid]);
		n.left = createBST(arr, lo, mid-1);
		n.right = createBST(arr, mid+1, hi);
		return n;
	}
}
