public class LCA
{
	/*public static Node getLCA(Node root, Node p, Node q)
	{
		// follow till paths diverge...that is LCA
		Node x = root;
		if (!covers(x, p) || !covers(x, q))	return null;
		while (x != null)
		{
			// check if left covers
			if (covers(x.left, p) && covers(x.left, q))
				x = x.left;
			else if (covers(x.right, p) && covers(x.right, q))
				x = x.right;
			else
				return x;
		}
	}*/
	
	public static Node getLCA(Node x, Node p, Node q)
	{
		// recursive implementation
		if (covers(x.left, p) && covers(x.left, q))
			return getLCA(x.left, p, q);
		else if (covers(x.right, p) && covers(x.right, q))
			return getLCA(x.right, p, q);
		return root;
	}

	private boolean covers(Node x, Node y)
	{
		if (x == null)	return false;
		if (x.equals(y))	return true;
		return (covers(x.left, y) || covers(x.right, y));
	}
}
