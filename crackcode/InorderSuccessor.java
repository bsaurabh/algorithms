public class InorderSuccessor
{
	// Assumption: Links to parents are present
	public static Node getSuccessor(Node x)
	{
		if (x == null)	return null;
		// found right child
		if (x.right != null || x.parent == null)
			return leftMostChild(x.right);
		else{
			// move up till we are on left side
			Node p;
			while ((p = x.parent) != null)
			{
				if (p.left == x)	break;
				x = p;
			}
			return p;
		}
		return null;
	}

	private static Node leftMostChild(Node x)
	{
		if (x == null)	return null;
		while (x.left != null)
			x = x.left;
		return x;
	}
}
