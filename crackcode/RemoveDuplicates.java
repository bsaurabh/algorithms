public class RemoveDuplicates
{
	public static void removeDuplicates(char[] str)
	{
		if (str == null)	return null;
		int len = str.length;
		if (len < 2)	return str;

		int tail = 1;
		for (int i = 1; i < len; ++1)
		{
			int j;
			for (j = 0; j < tail; ++j)
			{
				if (str[i] == str[j]	break;
				if (j == tail)
				{
					str[tail++] = str[i];
				}
			}
		}
		str[tail] = 0;
	}
}
