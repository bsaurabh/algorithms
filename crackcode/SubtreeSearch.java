public class SubtreeSearch
{
	public boolean isSubtree(Node t1, Node t2)
	{
		if (t2 == null)	return true;
		return subTree(t1, t2);
	}

	private boolean subTree(Node t1, Node t2)
	{
		if (t1 == null)
			return false;
		if (t1.item == t2.item)
			return matchTree(t1, t2);
		else
			return subTree(t1.left, t2)||subTree(t1.right, t2);
	}

	private boolean matchTree(Node t1, Node t2)
	{
		if (t2 == null && t1 == null)	return true;
		if (t1 == null || t2 == null)	return false;
		if (t1.item != t2.item)
			return false;
		return (matchLeft(t1.left, t2.left) && matchTree(t1.right, t2.right));
	}
}
