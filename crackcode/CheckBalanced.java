public class CheckBalanced
{
	public static boolean isBalanced(Node root)
	{
		int maxDepth = maxDepth(root);
		int minDepth = minDepth(root);
		return maxDepth - minDepth <= 1;
	}

	private int maxDepth(Node x)
	{
		if (x == null)	return 0;
		return Math.max(maxDepth(x.left), maxDepth(x.right))+1;
	}

	private int minDepth(Node x)
	{
		if (x == null)	return 0;
		return Math.min(minDepth(x.left), minDepth(x.right))+1;
	}
}
