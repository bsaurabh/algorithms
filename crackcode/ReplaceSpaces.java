public class ReplaceSpaces
{
	public static void replace(String s)
	{
		char[] contents = s.toCharArray();
		int spaces = 0;
		for (int i = 0; i < contents.length; i++)
			if (contents[i].equals(' '))	spaces++;
		// each space is to be replaced by %20
		int newLen = contents.length + spaces*2, j = contents.length-1;
		char[] newContents = new char[newLen];
		for (int i = newLen-1; i >= 0; i--)
		{
			if (!contents[j].equals(' '))
				newContents[i--] = contents[j--];
			else
			{
				newContents[i--] = '0';
				newContents[i--] = '2';
				newContents[i--] = '%';
				j--;
			}
		
		}
		return new String(newContents);
	}
}
