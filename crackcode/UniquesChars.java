public class UniqueChars
{
	private boolean isUnique(String s)
	{
		// make a char array
		// assume ASCII strings
		boolean[] char_set = new boolean[256];
		for (int i = 0; i < s.length(); i++)
		{
			int ch = (int)(s.charAt(i));
			if (char_set[i])	return false;
			char_set[i] = true;
		}
		return true;
	}

	// other methods are:
	// 1. check each element ... then check others... O(n^2)
	// 2. If we can destroy string...sort and check adjacent O(nlgn)
}
