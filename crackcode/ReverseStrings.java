public class ReverseStrings
{
	// reverse C style strings
	public static void reverse(char *str)
	{
		char *end = str;
		while (*end != '\0')
			end++;
		--end;
		// begin swapping
		while (str < end)
		{
			char tmp = *str;
			*str++ = *end;
			*end-- = tmp;
		}
	}
}
