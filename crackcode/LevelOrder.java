public class LevelOrder
{
	// returns all nodes at a particular level
	public static ArrayList<LinkedList<Node>> getLevelOrder(Node root)
	{
		if (root == null)	return null;
		// initialise
		ArrayList<LinkedList<Node>> result = new ArrayList<LinkedList<Node>>();
		LinkedList<Node> list = new LinkedList<Node>();
		int level = 0;
		list.add(root);
		result.add(level, list);

		while (true)
		{
			list = new LinkedList<Node>();
			LinkedList<Node> prevLevel = result.get(level);
			
			for (int i = 0; i < prevLevel.size(); i++)
			{
				Node x = prevLevel.get(i);
				if (x == null)	continue;
				if (x.left != null)	list.add(x.left);
				if (x.right != null)	list.add(x.right);
			}
			if (list.size() > 0)
				result.add(++level, list);
			else	break;
		}
		return result;
	}
}
