public class Date implements Comparable<Date>
{
	private final int date, month, year;

	public Date(int d, int m, int y)
	{
		date = d;
		month = m;
		year = y;
	}

	public int compareTo(Date that)
	{
		if (this.year > that.year)	return +1;
		if (this.year < that.year)	return -1;
		if (this.month > that.month)	return +1;
		if (this.month < that.month) 	return -1;
		if (this.date > that.date)	return +1;
		if (this.date < that.date)	return -1;
		return 0;	// equal
	}

	public boolean equals(Object y)
	{
		if (y == this)
			return true;	// optimization...and reflexive
		if (y == null)
			return false;	// equals() needs this
		if (y.getClass() != this.getClass())
			return false;
		// cast
		Date that = (Date) y;

		// now check values
		if (this.date != that.date)	return false;
		if (this.month != that.month)	return false;
		if (this.year != that.year)	return false;
		return true;
	}

}
