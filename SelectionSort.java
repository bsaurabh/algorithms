public class SelectionSort
{
	// (N^2) compares
	// N exchanges
	// Quadratic time even if input is sorted
	public static void sort(Comparable[] a)
	{
		int N = a.length;
		for (int i = 0; i < N; i++)
		{
			int min = i;
			for (int j = i + 1; j < N; j++)
			{
				if (less(a[j], a[min]))	// compares
					min = j;
				exch (a, i, min);	// exchange
			}
		}
	}

	private int less (Comparable v, Comparable w)
	{
		return (v.compareTo(w) < 0);
	}

	private void exch (Comparable[] a, int i, int j)
	{
		Comparable swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}

}
