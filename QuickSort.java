public class QuickSort
{
	// In-place sorting in nlg(n) expected time
	// Stop partitioning at duplicate keys
	private static int partition(Object[] a, int lo, int hi, Comparator c)
	{
		int i = lo, j = hi+1;
		// use a[lo] as partitioning element as array is shuffled before-hand
		// make a probablistic performance improvement
		// try and get partition element somewhere in middle
		int m = medianOf3(a, lo, (lo+hi)/2, hi);
		// swap a[lo] with a[m]
		exch(a, lo, m);	// about 10% performance improvement
		while(true)
		{
			while (less(a[++i], a[lo], c))
				if (i == hi)	break;	// advance i pointer till a[i] < a[lo]
			while (less(a[lo], a[--j], c))
				if (j == lo)	break;	// decrement j pointer till a[j]>a[lo]
			if (i >= j)	break;	// have the pointers crossed?
			exch(a, i, j);
		}
		exch(a, j, lo);	// put partition element in place
		return j;
	}

	private static boolean less(Object v, Object w, Comparator c)
	{
		return (c.compare(v, w) < 0);
	}

	private static void exch(Object[] a, int i, int j)
	{
		Object swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}

	public static void sort(Object[] a, Comparator c)
	{
		// Do a Knuth Shuffle
		// We are not choosing a random pivot
		// Expected complexity: nlg(n)
		KnuthShuffle.shuffle(a);
		// pass shuffled array to recursive routine
		sort(a, 0, a.length-1, c);
	}

	private static void sort(Object[] a, int lo, int hi, Comparator c)
	{
		if (hi <= lo)	break;	// nothing to do
		int CUTOFF = 10;	// running time improvement by 20%
		if (hi <= lo + CUTOFF - 1){
			InsertionSort.sort(a, lo, hi);
			return;
		}
		int r = partition(a, lo, hi, c);
		// recursively sort sub-arrays
		sort(a, lo, r-1, c);
		sort(a, r+1, hi, c);
	}
}
