import java.util.*;
public class StackOfStringsClient
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		// initialise stack
		//LinkedStackOfStrings stack = new LinkedStackOfStrings();
		FixedCapacityStackOfStrings stack = new FixedCapacityStackOfStrings(10);
		String inp = sc.nextLine();
		while (!inp.equals(""))
		{
			if (inp.equals("-"))
				System.out.println("> " + stack.pop());
			else
				stack.push(inp);
			inp = sc.nextLine();
		}
	}
}
