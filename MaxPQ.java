public class MaxPQ <Key extends Comparable<Key>>
{
	private Key[] pq;
	private int N;

	public MaxPQ()
	{
		pq = (Key[]) new Comparable[2];	// start using indices from 1
		N = 0;	// insertion happens at next element
	}

	// array helpers
	private void swim(int k)
	{
		while (k > 1 && less(k/2, k))
		{
			exch(k, k/2);
			k = k/2;
		}
	}

	private void sink(int k)
	{
		while (2*k <= N)
		{
			int j = 2*k;
			if (j < N && less(j, j+1))
				j++;	// j now points to bigger child
			if (!(less(k, j)))
				break;
			exch(k, j);
			k = j;
		}
	}

	// public API to access data structure
	public void insert(Key x)
	{
		// insert at end
		if (N == pq.length-1)
			resize(pq.length * 2); 
		pq[++N] = x;
		swim(N);
	}

	public Key delMax()
	{
		Key max = pq[1];
		exch(1, N--);
		sink(1);
		pq[N+1] = null;	// prevent loitering
		if(N == pq.length/4)
			resize(pq.length/2);
		return max;
	}

	// more helper methods
	private boolean less(int i, int j)
	{
		return (pq[i].compareTo(pq[j]) < 0);
	}

	private void exch(int i, int j)
	{
		Comparable swap = pq[i];
		pq[i] = pq[j];
		pq[j] = swap;
	}

	private void resize(int capacity)
	{
		Key[] aux = (Key[]) new Comparable[capacity];
		for (int i = 0; i <= N; i++)
			aux[i] = pq[i];
		pq = aux;
	}
}
