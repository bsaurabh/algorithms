public class DepthFirstOrder
{
	// topological sorting for Directed Acyclic Graphs (DAGs)
	// topological sorting does not work if a cycle exists
	private boolean[] marked;
	private Stack<Integer> reversePost;	// stack to keep postOrder

	public DepthFirstOrder(Digraph G)
	{
		int v = G.getVertexCount();
		marked = new boolean[v];
		reversePost = new Stack<Integer>();

		for (int i = 0; i < v; i++)
		{
			if (!marked[i])
				dfs(G, i);
		}
	}

	private void dfs(Digraph G, int v)
	{
		marked[v] = true;
		for (int w: G.adj(v))
			if (!marked[w])
				dfs(G, w);
		reversePost.push(v);
	}

	public Iterable<Integer> reversePost()
	{
		return reversePost;
	}
}
