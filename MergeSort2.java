public class MergeSort2
{
	private static void merge(Object[] a, Object[] aux, int lo, int mid, int hi, Comparator c)
	{
		// copy all elements into aux
		for (int i = lo; i <= hi; i++)
			aux[i] = a[i];
		int i = lo, j = mid+1;	// pointers to sub-arrays
		for (int k = lo; k <= hi; k++)
		{
			if (i > mid)	a[k++] = aux[j++];
			else if (j > hi)	a[k++] = aux[i++];
			else if (less(a[i], a[j], c))	a[k++] = aux[i++];
			else	a[k++] = aux[j++];
		}
	}


	private static void sort (Object[] a, Object[] aux, int lo, int hi, Comparator c)
	{
		if (hi <= lo)
			return;	// nothing to do
		int CUTOFF = 7;
		if (hi <= lo + CUTOFF - 1)
			InsertionSort.sort(a, lo, hi);	// efficiency
		int mid = (hi + lo)/2;
		sort(a, aux, lo, mid, c);
		sort(a, aux, mid+1, hi, c);
		if (less(a[mid], a[mid+1], c))
			return;	// already sorted
		merge(a, aux, lo, mid, hi, c);
	}

	public static void sort(Object[] a, Comparator c)
	{
		Object[] aux = new Object[a.length];
		sort(a, aux, 0, a.length-1, c);
	}

	private static boolean less(Object i, Object j, Comparator c)
	{
		return (c.compare(i, j) < 0);
	}

	private static void exch(Object[] a, int i, int j)
	{
		Object swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}

}
