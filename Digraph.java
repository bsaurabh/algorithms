public class Digraph
{
	private final int V;
	private final Bag<Integer>[] adj; // adjacency list stores bags

	public Digraph(int V)
	{	
		this.V = V;
		adj = new Bag<Integer>[V];

		// initilialise each bag
		for (int i = 0; i < V; i++)
			adj[i] = new Bag<Integer>();
	}

	public void addEdge(int v, int w)
	{
		adj[v].add(w);
		// reverse not applicable here (compare with Graph.java)
	}

	public Iterable<Integer> adj(int v)
	{
		// returns the adjacency list of vertex v
		return adj[v];
	}
}
