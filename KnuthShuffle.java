import java.util.Random;
public class KnuthShuffle
{
	public static void shuffle(Comparable[] a)
	{
		int N = a.length;
		Random rand = new Random();
		for (int i = 0; i < N; i++)
		{
			// generate random number r in [0,i]
			int r = rand.nextInt(i + 1);
			// swap a[i] and a[r]
			exch(a, i, r);
		}
	}
	private static void exch (Comparable[] a, int i, int j)
	{
		Comparable swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}
}
