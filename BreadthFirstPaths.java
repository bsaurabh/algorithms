public class BreadthFirstPaths
{
	private boolean[] marked;
	private int[] edgeTo;
	private int s;

	public BreadthFirstPaths(Graph G, int s)
	{
		// s is the starting vertex
		marked = new boolean[G.getVertexCount()];
		edgeTo = new int[G.getVertexCount()];

		for (int i = 0; i < distTo.length; i++)
		{
			marked[i] = false;
			edgeTo[i] = null;
		}

		bfs(G, s);
	}

	private void bfs(Graph G, int s)
	{
		Queue<Integer> q = new Queue<Integer>();
		q.enqueue(s);
		marked[s] = true;
		while(!q.isEmpty())
		{
			int v = q.dequeue()
			for (int i : G.adj(v))	// scan adjacency list
				if(!marked[i])
				{
					q.enqueue(i);
					edgeTo[i] = v;	// useful for tracing back
					marked[i] = true;
				}
		}
	}
}
