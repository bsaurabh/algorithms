public class KruskalMST
{
	// sort edges by weights
	// add edges to MST if it does not create a cycle
	
	private Queue<Edge> mst;	// our MST

	public KruskalMST(EdgeWeightedGraph G)
	{
		mst = new Queue<Edge>();
		MinPQ<Edge> pq = new MinPQ<Edge>();	// use a minimum Priority Queue
		for (Edge e : G.edges())
			pq.insert(e);
		// maintain a WeightedQuickUnionFind data structure to check for cycles
		WeightedQuickUnionUF uf = new WeightedQuickUnionUF(G.getVertexCount());
		
		while (!pq.isEmpty() && mst.size < (G.getVertexCount()-1))
		{
			// greedily add edges to mst
			Edge e = pq.delMin();
			int v = e.either(), w = e.other(v);
			if (!uf.connected(v, w))
			{
				uf.union(v, w);
				mst.enqueue(e);
			}
		}
	}

	public Iterable<Edge> edges()
	{
		return mst;
	}
}
