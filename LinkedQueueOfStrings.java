public class LinkedQueueOfStrings
{
	private class Node
	{
		String item;
		Node next;
	}

	private Node first, last;
	first = null;
	last = null;

	public boolean isEmpty()
	{
		return (first == null);
	}

	public void enqueue(String str)
	{
		Node oldLast = last;
		last = new Node();
		last.item = str;
		last.next = null;

		if (isEmpty())
			first = last;
		else
			oldLast.next = last;
	}

	public String dequeue()
	{
		String item = first.item;
		first = first.next;
		if (isEmpty())
			last = null;	// for empty queue
		return item;
	}

}
