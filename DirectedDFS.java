public class DirectedDFS
{
	private boolean[] marked;
	private int[] edgeTo;

	public DirectedDFS(Digraph G, int s)
	{
		marked = new boolean[G.getVertexCount()];
		edgeTo = new int[G.getVertexCount()];

		for (int i = 0; i < marked.length; i++)
		{
			marked[i] = false;
			edgeTo[i] = null;
		}

		dfs(G, s);
	}

	private void dfs(Digraph G, int v)
	{
		marked[v] = true;
		for (int w: G.adj(v))
			if (!marked[w])
			{
				dfs(G, w);
				edgeTo[w] = v;
			}
	}

	public boolean visited(int v)
	{
		return marked[v];
	}
}
