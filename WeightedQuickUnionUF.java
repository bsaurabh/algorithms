public class WeightedQuickUnionUF
{
	private int id[];
	private int sz[];	// stores size of tree rooted at i

	public WeightedQuickUnionUF(int N)
	{
		// initialise array with nodes rooted at themselves
		id = new int[N];
		sz = new int[N];
		for (int i = 0; i < N; i++){
			id[i] = i;
			sz[i] = 1;
		}
	}

	private int root (int p)
	{
		int i = p;
		while (i != id[i])
		{
			id[i] = id[id[i]];	// path compression
			i = id[i];	// make i = i's parent
		}
		return i;
	}

	public boolean connected (int p, int q)
	{
		return (root(p) == root(q));
	}

	public void union(int p, int q)
	{
		int p_root = root(p);
		int q_root = root(q);
		if (sz[p_root] < sz[q_root])
		{
			id[p_root] = q_root;
			sz[q_root] += sz[p_root];
		}
		else
		{
			id[q_root] = p_root;
			sz[p_root] += sz[q_root];
		}
	}
}
