public class OrderStatistics
{
	// Quick select algorithm
	// linear expected running time
	public static Comparable select(Comparable[] a, int k)
	{
		// shuffle the array to ensure randomness
		KunthShuffle.shuffle(a);
		// use quicksort partition to find element
		while (hi > lo)
		{
			int r = QuickSort.partition(a, 0, a.length-1);	// make partition public
			if (k > r)
				lo = r+1;	// search right sub-array
			else if(k < r)
				hi = r-1;	// search left sub-array
			else
				return a[r];
		}
		return a[r];
	}
}
