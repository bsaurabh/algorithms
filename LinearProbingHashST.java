public class LinearProbingHashST<Key, Value>
{
	private int M = 30001;	// number of Key,Value pairs
	private Key[] keys = (Key[]) new Object[M];
	private Value[] values = (Value[]) new Object[M];

	private int hash(Key key)
	{
		return (key.hashCode() & 0x7fffffff) % M;
	}

	public Value get(Key key)
	{
		for (int i = hash(key); keys[i] != null; i = (i+1)%M)	// when running off end
			if (keys[i].equals(key))
				return values[i];
		return null;
	}

	public void put(Key key, Value val)
	{
		for (int i = hash(key); keys[i] != null; i = (i+1)%M)
			if (key.equals(keys[i]))
				break;
		keys[i] = key;
		values[i] = val;
	}

}
