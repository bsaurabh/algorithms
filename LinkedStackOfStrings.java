public class LinkedStackOfStrings
{
	private class Node
	{
		String item;
		Node next;
	}

	private Node first = null;

	public boolean isEmpty()
	{
		return (first == null);
	}

	public void push(String str)
	{
		// keep track of first
		Node oldFirst = first;

		// create a new Node and link to oldFirst
		first = new Node();
		first.item = str;
		first.next = oldFirst;
	}

	public String pop()
	{
		if (isEmpty())
			return "";
		// get the item from node to be deleted
		String item = first.item;
		// advance first
		first = first.next;
		return item;
	}
}
