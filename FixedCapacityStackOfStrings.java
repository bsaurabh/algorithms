public class FixedCapacityStackOfStrings
{
	private String s[];
	private int N;	// used in place of first pointer

	public FixedCapacityStackOfStrings(int capacity)
	{
		s = new String[capacity];
		N = 0;
	}

	public boolean isEmpty()
	{
		return (N == 0);
	}

	public void push(String str)
	{
		s[N++] = str;	// N keeps track of position where insertion is to happen
	}

	public String pop()
	{
		if (isEmpty())
			return "";
		String item = s[--N];
		s[N] = null;
		return item;
	}

}
