import java.util.*;
public class UFClient
{
	public static void main (String[] args)
	{
		Scanner sc = new Scanner(System.in);
		// take number of numbers
		int N;
		N = sc.nextInt();

		// initialise UF data structure
		WeightedQuickUnionUF uf = new WeightedQuickUnionUF(N);

		// now read inputs
		while (sc.hasNextLine())
		{
			// will stop if Ctrl+D is input
			int p, q;
			p = sc.nextInt();
			if (p == -999)
				break;
			q = sc.nextInt();
			// connect (p, q) if NOT connected
			if (!uf.connected(p, q))
			{
				uf.union(p, q);
				System.out.println("Union > " + p + " " + q);
			}
			else
				System.out.println("Connected > " + p + " " + q);
		}
	}

}
