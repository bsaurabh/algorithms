public class Crawler
{
	// simple web crawler
	private Queue<String> q = new Queue<String>();
	private SET<String> discovered = new SET<String>();	// set of visited websited

	public Crawler(String s)
	{
		// s is root website
		q.enqueue(s);
		discovered.add(s);
	}

	public void crawl()
	{
		while(!q.isEmpty())
		{
			String v = q.dequeue();

			// read HTML file
			InputStreamReader isr = new InputStreamReader(v);
			BufferedReader br = new BufferedReader(isr);
			String html = br.readLines();

			// get all URLs use regex
			String regexp = "http://(\\w+\\.)*(\\w+)";
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(html);

			while(matcher.find())
			{
				String w = matcher.group();
				while (!discovered.contains(w))
				{
					discovered.add(w);
					q.enqueue(w);
				}
			}
		}
	}
}
