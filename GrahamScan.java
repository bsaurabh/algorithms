public class GrahamScan
{
	public static Stack<Point2D> scan(Point2D points[])
	{
		// maintain a push down stack of points
		Stack<Point2D> hull = new Stack<Point2D>();

		Arrays.sort(points, Point2D.Y_ORDER);
		Arrays.sort(points, points[0].POLAR_ORDER);	// sort by polar angle relative to p[0]
		hull.push(p[0]);
		hull.push(p[1]);	// definitely on hull

		for (int i = 2; i < points.length; i++)
		{
			Point2D top = hull.pop();
			while (Point2D.ccw(hull.peek(), top, points[i]) <= 0)	// clockwise
				top = hull.pop();
			hull.push(top);
			hull.push(points[i]);
		}
	}

}
