public class RedBlackBST <Key extends Comparable<Key>, Value>
{
	private Node root;
	private static final boolean RED = true;
	private static final boolean BLACK = false;

	private class Node
	{
		Key key;
		Value val;
		int count;
		Node left, right;
		boolean color;	// color of link coming into this node from parent

		public Node(Key key, Value val, boolean color)
		{
			this.key = key;
			this.val = val;
			this.color = color;
		}
	}

	// API
	public int size()
	{
		return size(root);
	}

	public void put(Key key, Value val)
	{
		root = put(root, key, val);
	}

	public Value get(Key key)
	{
		Node x = root;
		while (x != null)
		{
			int cmp = key.compareTo(x.key);
			if (cmp < 0)
				x = x.left;
			else if (cmp > 0)
				x = x.right;
			else
				return x.val;
		}
		return null;
	}

	public Key floor(Key key)
	{
		Node x = floor(root, key);
		if (x == null)	return null;
		return x.key;
	}

	public Key ceil(Key key)
	{
		Node x = ceil(root, key);
		if (x == null)	return null;
		return x.key;
	}

	public int rank (Key key)
	{
		return rank(root, key);	
	}

	public int size(Key lo, Key hi)
	{
		// 1-D range search
		if (get(hi) != null)	// hi is there in the Symbol Table
			return 1 + rank(hi) - rank(floor(lo));
		else
			return rank(floor(hi)) - rank(floor(lo));
	}

	// Standard rotations
	private Node rotateLeft(Node h)
	{
		// rotates a right-leaning RED link to left-leaning
		assert isRed(h.right);

		Node x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = h.color;
		h.color = RED;
		return x;	// parent can now link to this
	}

	private Node rotateRight(Node h)
	{
		// rotate a left-leaning RED link to right leaning
		assert isRed(h.left);

		Node x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		return x;
	}

	private Node flipColors(Node h)
	{
		// to split a temporary 4-node
		assert !isRed(h);
		assert isRed(h.left);
		assert isRed(h.right);

		// just change all colors
		h.left.color = BLACK;
		h.right.color = BLACK;
		h.color = RED;
	}

	// API helpers
	
	private int rank(Node x, Key key)
	{
		if (x == null)	return 0;
		int cmp = key.compareTo(x.key);
		if (cmp == 0)
			return size(x.left);
		if (cmp < 0)
			return rank(x.left, key);
		return (1 + size(x.left) + rank(x.right, key));
	}

	private Node put(Node x, Key key, Value val)
	{
		if (x == null)
			return new Node(key, val, RED);
		int cmp = key.compareTo(x.key);
		if (cmp < 0)
			x.left = put(x.left, key, val);
		else if (cmp > 0)
			x.right = put(x.right, key, val);
		else
			x.val = val;

		// now do rotations
		if (isRed(x.right) && !isRed(x.left))
			x = rotateLeft(x);
		if (isRed(x.left) && isLeft(x.left.left))
			x = rotateRight(x);
		if (isRed(x.left) && isRed(x.right))
			flipColors(x);

		// do count calculations
		x.count = 1 + size(x.left) + size(x.right);
		return x;
	}

	private int size(Node x)
	{
		if (x == null)
			return 0;
		return x.count;
	}

	private Node floor(Node x, Key key)
	{
		if (x == null)
			return null;
		int cmp = key.compareTo(x.key);
		if (cmp < 0)
			return floor(x.left, key);
		else if (cmp == 0)
			return x;
		// consider right sub-tree
		Node t = floor(x.right, key);
		if (t != null)
			return t;
		return x;
	}

	private Node ceil(Node x, Key key)
	{
		if (x == null)
			return null;
		int cmp = key.compareTo(x.key);
		if (cmp > 0)
			return ceil(x.right, key);
		else if (cmp == 0)
			return x;
		// consider left sub-array
		Node t = ceil(x.left, key);
		if (t != null)	return t;
		return x;
	}

	private boolean isRed(Node x)
	{
		if (x == null)
			return false;	// all null nodes are linked by BLACK
		return (x.color == RED);
	}
}
