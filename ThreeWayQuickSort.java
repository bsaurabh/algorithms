public class ThreeWayQuickSort
{
	// A much better quicksort
	// resolves issue of duplicate keys
	// lg (N!/(x1!*x2!*x3!...))
	// This is linear when all elements are same...Bazinga!
	public static void sort(Comparable[] a, int lo, int hi)
	{
		if (hi <= lo)
			return;
		int CUTOFF = 10;
		if (hi <= lo + CUTOFF - 1)
			InsertionSort.sort(a, lo, hi);
		// Choose a[lo] as partition element
		int lt = lo, gt = hi;	// pointers to boundaries of a[k] == a[lo]
		Comparable v = a[lo];
		int i = lo;	// pointer
		while (i <= gt)
		{
			int cmp = a[i].compareTo(v);
			if (v < 0)
				exch(a, lt++, i++);
			else if (v > 0)
				exch(a, i, gt--);
			else
				i++;	// equal case
		}
		// now partitioning is done
		sort(a, lo, lt-1);
		sort(a, gt+1, hi);
	}
}
