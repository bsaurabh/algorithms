public class DepthFirstPaths
{
	private boolean[] marked;
	private int[] edgeTo;
	private int s;	// source vertex

	public DepthFirstPaths(Graph G, int s)
	{
		// initialisation
		this.s = s;
		marked = new boolean[G.getVertexCount()];
		edgeTo = new int[G.getVertexCount()];
		for (int i = 0; i < marked.length; i++)
		{
			marked[i] = false;
			edgeTo[i] = null;
		}

		// call dfs recursive routine
		dfs(G, s);
	}

	private void dfs(Graph G, int v)
	{
		marked[v] = true;
		for (int w:G.adj(v))
		{
			if (!marked[w])
			{
				dfs(G, w);
				edgeTo[w] = v;
			}
		}
	}

	public boolean hasPathTo(int v)
	{
		// constant time
		return marked[v];
	}

	public Iterable<Integer> pathTo(int v)
	{
		// linear time (proportional to path length)
		if (!hasPathTo(v))
			return null;
		Stack<Integer> path = new Stack<Integer>();
		for (int x = v; x != s; x = edgeTo[x])
			path.push(x);
		path.push(s);
		return path;
	}
}
