public class Edge implements Comparable<Edge>
{
	// represents Edges of an edge-weighted graph
	// vertices
	private int v, w;
	private double weight;

	public Edge(int v, int w, double weight)
	{
		this.v = v;
		this.w = w;
		this.weight = weight;
	}

	public int either()
	{
		return v;
	}

	public int other(int v)
	{
		if (this.v == v)	return w;
		return v;
	}

	public int compareTo(Edge that)
	{
		if (this.weight > that.weight)	return +1;
		if (this.weight < that.weight)	return -1;
		return 0;
	}
}
