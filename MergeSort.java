public class MergeSort
{
	private static void merge(Comparable[] a, Comparable[] aux, int lo, int mid, int hi)
	{
		assert isSorted(a, lo, mid);	// precondition a[lo...mid] sorted
		assert isSorted(a, mid+1, hi);	// precondition a[mid+1...hi] sorted

		// copy a[] into aux[]
		for (int i = lo; i <= hi; i++)
			aux[i] = a[i];
		int i = lo, j = mid+1;	// pointing to 1st elements of sorted sub-arrays
		for (int k = lo; k <= hi; k++)
		{
			if (i > mid)	a[k++] = aux[j++];
			else if (j > hi)	a[k++] = aux[i++];
			else if (less(aux[i], aux[j]))	a[k++] = aux[i++];
			else	a[k++] = aux[j++];
		}

		assert isSorted(a, lo, hi);	// postcondition a[lo...hi] sorted
	}

	private boolean isSorted(Comparable[] a, int lo, int hi)
	{
		for (int i = 0; i < hi-1; i++)
			if(!(less(a[i], a[i+1])))
				return false;
		return true;
	}

	private static void sort(Comparable[] a, Comparable[] aux, int lo, int hi)
	{
		// recursive routine
		if (hi <= lo)
			return;	// nothing to do
		int CUTOFF = 7;
		if (hi <= lo + CUTOFF - 1)
			InsertionSort.sort(a, lo, hi);
		int mid = lo + (hi - lo)/2;
		sort(a, aux, lo, mid);
		sort(a, aux, mid+1, hi);
		// stop of already sorted
		if ((less(a[mid], a[mid+1])))
			return;
		merge(a, aux, lo, mid, hi);
	}

	public static void sort(Comparable[] a)
	{
		Comparable[] aux = new Comparable[a.length];
		sort(a, aux, 0, a.length-1);
	}

}
