public class UnorderedMaxPQ <Key extends Comparable<Key>>
{
	private Key[] pq;	// the priority Queue
	private int N;
	
	public UnorderedMaxPQ()
	{
		pq = (Key[])new Comparable[1];
		N = 0;	// place where insertion happens next
	}

	private void resize(int capacity)
	{
		Key[] aux = (Key[])new Comparable[capacity];
		for (int i = 0; i < N; i++)
			aux[i] = pq[i];
		pq = aux;	// old PQ reclaimed by garbage collector
	}

	public void insert(Key key)
	{
		if (N == pq.length)
			resize(N*2);
		pq[N++] = key;
	}

	public boolean isEmpty()
	{
		return (N == 0);
	}

	public Key delMax()
	{
		int max = 0;
		for (int i = 1; i < N; i++)
			if (less(pq[max], pq[i]))
				max = i;
		// swap max element with ending element
		exch(pq, max, N-1);
		Key item = pq[N--];
		pq[N] = null;	// prevent loitering
		if (N == pq.length/4)
			resize(pq.length/2);
		return item;
	}

	private boolean less(Comparable v, Comparable w)
	{
		return (v.compareTo(w) < 0);
	}

	private void exch(Comparable[] a, int i, int j)
	{
		Comparable swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}
}
