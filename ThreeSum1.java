import java.util.*;
public class ThreeSum1
{
	// Brute force algorithm
	
	private static int count (int a[])
	{
		int N = a.length;
		int count  = 0;
		for (int i = 0; i < N; i++)
			for (int j = i+1; j < N; j++)
				for (int k = j + 1; k < N; k++)
					if ((a[i] + a[j] + a[k]) == 0)
						count++;
		return count;
	}
	
	public static void main(String[] args)
	{
		// read integers into a[]
		Scanner sc  = new Scanner (System.in);
		// get number of elements
		int N = sc.nextInt();
		int a[] = new int[N];
		for (int i = 0; i < N; i++)
			a[i] = sc.nextInt();
		System.out.println(count(a));
	}
	
}
