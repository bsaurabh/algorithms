public class EdgeWeightedDigraph
{
	private final int V;
	private final Bag<DirectedEdge>[] adj;

	public EdgeWeightedDigraph(int V)
	{
		this.V = V;
		adj = new Bag<DirectedEdge>[V];

		for (int i = 0; i < V; i++)
			adj[i] = new Bag<DirectedEdge>();
	}

	public void addEdge(DirectedEdge e)
	{
		int v = e.from();
		adj[v].add(e);
	}

	public Iterable<DirectedEdge> adj(int v)
	{
		return adj[v];
	}
}
