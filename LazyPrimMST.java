public class LazyPrimMST
{
	private boolean[] marked;
	private Queue<Edge> mst;
	private MinPQ<Edge> pq;

	public LazyPrimMST(EdgeWeightedGraph G)
	{
		pq = new MinPQ<Edge>();
		mst = new Queue<Edge>();
		marked = new boolean[G.getVertexCount()];

		visit(G, 0);	// assume G is connected

		while (!pq.isEmpty())
		{
			Edge e = pq.delMin();
			int v= e.either(), w = e.other(v);
			if (marked[v] && marked[w]) 	continue;
			if (marked[v])	visit(G, w);
			else visit(G, v);
		}
	}

	private void visit(EdgeWeightedGraph G, int v)
	{
		marked[v] = true;
		for (Edge e : G.adj(v))
			if (!marked(e.other(v)))
				pq.insert(e);
	}

	public Iterable<Edge> edges()
	{
		return mst;
	}
}
