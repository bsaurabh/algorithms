public class EdgeWeightedGraph
{
	private final int V;
	private final Bag<Edge>[] adj;

	public EdgeWeightedGraph(int V)
	{
		this.V = V;
		this.adj = new Bag<Edge>[V];

		// initialise bags for every vertex
		for (int i = 0; i < this.V; i++)
			adj[i] = new Bag<Edge>();
	}

	public void addEdge(Edge e)
	{
		int v = e.either(), w = e.other(v);
		adj[v].add(e);
		adj[w].add(e);
	}

	public Iterable<Edge> adj(int v)
	{
		return adj[v];
	}
}
