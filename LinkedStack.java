import java.util.Iterator;
public class LinkedStack <Item> implements Iterable <Item>
{
	/* Since we are implementing Iterable
	 * We have to implement iterator()
	 */

	public Iterator<Item> iterator()
	{
		return new ListIterator();
	}

	private class ListIterator <Item> implements Iterator <Item>
	{
		private Node current = first;

		public boolean hasNext()
		{
			return (current != null);
		}

		public Item next()
		{
			Item item = current.item;
			current = current.next();
			return item;
		}

		public void remove()
		{
			// Not supported
		}
	}
	private class Node
	{
		Item item;
		Node next;
	}
	
	private Node first = null;

	public boolean isEmpty()
	{
		return (first == null);
	}

	public void push(Item item)
	{
		Node oldFirst = first;
		first = new Node();
		first.item = item;
		first.next = oldFirst;
	}

	public Item pop()
	{
		Item item = first.item;
		first = first.next;
		return item;
	}
}
