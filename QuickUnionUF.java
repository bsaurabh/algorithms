public class QuickUnionUF
{
	private int id[];

	public QuickUnionUF(int N)
	{
		id = new int[N];
		for (int i = 0; i < N; i++)
			id[i] = i;
	}

	private int root(int p)
	{
		int i = p;
		while (i != id[i])
			i = id[i];	// make it point to parent
		return i;
	}

	public boolean connected (int p, int q)
	{
		return (root(p) == root(q));
	}

	public void union(int p, int q)
	{
		int p_root = root(p);
		int q_root = root(q);
		id[p_root] = q_root;
	}

}
