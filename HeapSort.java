public class HeapSort
{
	public static void sort(Comparable[] pq)
	{	
		// build a max heap out of the array
		int N = pq.length;
		for (int k = N/2; k >= 1; k--)
			sink(pq, k, N);
		// use the heap to sort
		while (N > 1)
		{
			exch(pq, 1, N--);
			sink(pq, 1, N);
		}
	}

	private static void sink(Comparable[] pq, int k, int N)
	{
		while (2*k <= N)
		{
			int j = 2*k;
			if (j < N && less(pq, j, j+1))
				j++;
			if(!less(pq, k, j))
				break;
			exch(pq, k, j);
			k = j;
		}
	}

	private static boolean less(Comparable[] pq, int i, int j)
	{
		return (pq[i-1].compareTo(pq[j-1]) < 0);
		// i-1 and j-1 taken as here array is not stored from 1...N
		// index 0 is also used
	}

	private static void exch(Comparable[] pq, int i, int j)
	{
		Comparable swap = pq[i];
		pq[i] = pq[j];
		pq[j] = swap;
	}
}
