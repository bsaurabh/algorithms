public class ShortestPath
{
	private int[] distTo;
	private DirectedEdge[] edgeTo;

	public double distTo(int v)
	{
		return distTo[v];
	}

	public Iterable<DirectedEdge> pathTo(int v)
	{	
		Stack<DirectedEdge> path = new Stack<DirectedEdge>();
		for (DirectedEdge e = edgeTo[v]; e != null; e = edgeTo[e.from()])
			path.push(e);
		return path;
	}

	private void relax(DirectedEdge e)
	{
		int v = e.from(), w = e.to();
		if (distTo[w] > distTo[v] + e.weight)
		{
			// relax this edge
			distTo[w] = distTo[v] + e.weight;
			edgeTo[w] = e;
		}
	}
}
