public class KosarajuSharirSCC
{
	// find strongly connected components
	private boolean[] marked;
	private int[] id;
	private int count;

	public KosarajuSharirSCC(Digraph G)
	{
		int v = G.getVertexCount();
		marked = new boolean[v];
		id = new int[v];
		count = 0;

		// Phase 1: Get reverse postorder on reversed digraph
		DepthFirstOrder dfs = new DepthFirstOrder(G.reverse());	// topological sort
		// Phase 2: Use reverse post order as DFS order on original graph
		for (int w: dfs.reversePost())
		{
			if(!marked[w])
			{
				dfs(G, w);
				count++;
			}
		}
	}

	private void dfs(Digraph G, int v)
	{
		marked[v] = true;
		id[v] = count;
		for (int w : G.adj(w))	// outgoing adjacency list considered
			if (!marked[w])
				dfs(G, w);
	}

	public boolean stronglyConnected(int v, int w)
	{
		return (id[v] == id[w]);
	}
}
