public class CC
{
	// pre-processes graph and assigns every connected componenet a unique ID
	private boolean[] marked;
	private int[] id;
	private int count;

	public CC(Graph G)
	{
		int v = G.getVertexCount();
		marked = new boolean[v];
		id = new int[v];
		count = 0;
		
		for (int i = 0; i < v; i++)
		{
			// for every non-marked vertex do a DFS
			if (!marked[i])
			{
				dfs(G, i);
				count++;
			}
		}
	}

	private void dfs(Graph G, int v)
	{
		// recursive DFS routine
		marked[v] = true;
		id[v] = count;
		for (int w:G.adj(v))
			if (!marked[w])
				dfs(G, w);
	}

	public int count()
	{
		// returns the number of connected components in the graph
		return count;
	}

	public int id(int v)
	{
		// returns the id of the connected component v is in
		return id[v];
	}
}
